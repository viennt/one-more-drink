export default {
	"welcome": "Chào mừng bạn tới",
	"app_name": "One More Drink",
	"warning": "Cảnh báo!!",
	"note1": "(1) Uống có trách nhiệm!",
	"note2": "(2) Đã uống rượu thì không uống bia... à không lái xe!",
	"disclaimer": "Người chơi chịu trách nhiệm với hành động của mình. App không chịu trách nhiệm các vấn đề phát sinh (nếu có).",
	"agree": "Đồng ý"
};