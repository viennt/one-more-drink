export default {
	"welcome": "Welcome to",
	"app_name": "One More Drink",
	"warning": "Warning !!",
	"note1": "(1) Drink Responsibly!",
	"note2": "(2) If you have been drink alcohol, Don't drive!",
	"disclaimer": "You are responsible for your actions. App is not responsible for problems that arise (if any)",
	"agree": "Agree"
};