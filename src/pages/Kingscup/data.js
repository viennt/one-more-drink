const useImage = (url) => {
  return new URL(`/src/pages/Kingscup/images/pokers/${url}`, import.meta.url).href;
};

const back = Array.from(Array(15).keys())
          .map(k => useImage(`back/backdesign_${k+1}.png`))

export default {
  back,
  cards: [
    { name: 'waterfall', image: useImage('A_diamonds.png') },
    { name: 'you', image: useImage('2_diamonds.png') },
    { name: 'me', image: useImage('3_diamonds.png') },
    { name: 'floor', image: useImage('4_diamonds.png') },
    { name: 'guy', image: useImage('5_diamonds.png') },
    { name: 'girls', image: useImage('6_diamonds.png') },
    { name: 'heaven', image: useImage('7_diamonds.png') },
    { name: 'mate', image: useImage('8_diamonds.png') },
    { name: 'rhyme', image: useImage('9_diamonds.png') },
    { name: 'category', image: useImage('10_diamonds.png') },
    { name: 'rule', image: useImage('J_diamonds.png') },
    { name: 'question', image: useImage('Q_diamonds.png') },
    { name: 'pour', image: useImage('K_diamonds.png') },

    { name: 'waterfall', image: useImage('A_spades.png') },
    { name: 'you', image: useImage('2_spades.png') },
    { name: 'me', image: useImage('3_spades.png') },
    { name: 'floor', image: useImage('4_spades.png') },
    { name: 'guy', image: useImage('5_spades.png') },
    { name: 'girls', image: useImage('6_spades.png') },
    { name: 'heaven', image: useImage('7_spades.png') },
    { name: 'mate', image: useImage('8_spades.png') },
    { name: 'rhyme', image: useImage('9_spades.png') },
    { name: 'category', image: useImage('10_spades.png') },
    { name: 'rule', image: useImage('J_spades.png') },
    { name: 'question', image: useImage('Q_spades.png') },
    { name: 'pour', image: useImage('K_spades.png') },

    { name: 'waterfall', image: useImage('A_hearts.png') },
    { name: 'you', image: useImage('2_hearts.png') },
    { name: 'me', image: useImage('3_hearts.png') },
    { name: 'floor', image: useImage('4_hearts.png') },
    { name: 'guy', image: useImage('5_hearts.png') },
    { name: 'girls', image: useImage('6_hearts.png') },
    { name: 'heaven', image: useImage('7_hearts.png') },
    { name: 'mate', image: useImage('8_hearts.png') },
    { name: 'rhyme', image: useImage('9_hearts.png') },
    { name: 'category', image: useImage('10_hearts.png') },
    { name: 'rule', image: useImage('J_hearts.png') },
    { name: 'question', image: useImage('Q_hearts.png') },
    { name: 'pour', image: useImage('K_hearts.png') },

    { name: 'waterfall', image: useImage('A_clubs.png') },
    { name: 'you', image: useImage('2_clubs.png') },
    { name: 'me', image: useImage('3_clubs.png') },
    { name: 'floor', image: useImage('4_clubs.png') },
    { name: 'guy', image: useImage('5_clubs.png') },
    { name: 'girls', image: useImage('6_clubs.png') },
    { name: 'heaven', image: useImage('7_clubs.png') },
    { name: 'mate', image: useImage('8_clubs.png') },
    { name: 'rhyme', image: useImage('9_clubs.png') },
    { name: 'category', image: useImage('10_clubs.png') },
    { name: 'rule', image: useImage('J_clubs.png') },
    { name: 'question', image: useImage('Q_clubs.png') },
    { name: 'pour', image: useImage('K_clubs.png') }
  ]
};
