import en from "./languages/en.json"
import vi from "./languages/vi.json"

export default {
	name: "kingscup",
	url: "/kingscup",
	image: new URL(`/src/pages/Kingscup/icon.png`, import.meta.url).href,
	languages: { en, vi }
}