import en from "./languages/en.json"
import vi from "./languages/vi.json"

export default {
	name: "wheel",
	url: "/wheel",
	image: new URL(`/src/pages/Wheel/icon.png`, import.meta.url).href,
	languages: { en, vi },
	sectors: [
	  { label: 'X', desc: "Không cần uống" },
	  { label: 'Đồng khởi', desc: "Tất cả cùng uống" },
	  { label: 'Hỏi', desc: "Được hỏi người khác 1 câu và tự uống" },
	  { label: '1', desc: "Uống 1 chén" },
	  { label: '2', desc: "Uống 2 chén" },
	  { label: 'Trái', desc: "Người bên trái uống" },
	  { label: 'Phải', desc: "Người bên phải uống" },
	  { label: 'Đối diện', desc: "Người đối diện uống" },
	  { label: 'Người đi kèm', desc: "Người đi cùng uống" },
	  { label: 'Chọn', desc: "Chọn 1 người uống" },
	  { label: 'Gái', desc: "Toàn bộ nữ uống" },
	  { label: 'Trai', desc: "Toàn bộ nam uống" }
	]
}