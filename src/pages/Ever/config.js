import en from "./languages/en.json"
import vi from "./languages/vi.json"

export default {
	name: "ever",
	url: "/ever",
	image: new URL(`/src/pages/Ever/icon.png`, import.meta.url).href,
	languages: { en, vi }
}