import { createRouter, createWebHashHistory } from "vue-router";
import Home from "../pages/Home/index.vue";
import getRoutes from "@/helper/routes";

export default createRouter({
  history: createWebHashHistory(),
  routes: [
    {
      path: "/",
      component: Home,
    },
    ...getRoutes()
  ],
});
