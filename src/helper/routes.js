function createRoute(slug, path, component, remoteComponent) {
	return {
		path: "/"+slug,
		name: slug.split("/")[0],
		component,
		remotePath: "./src/pages/"+path,
		remoteComponent
	}
}
const routes = [
	createRoute(
		"home", 
		"Home/home.vue", 
		() => import("@/pages/Home/home.vue"),
		() => import("remote_app/home")
	),
	createRoute(
		"wheel", 
		"Wheel/index.vue", 
		() => import("@/pages/Wheel/index.vue"),
		() => import("remote_app/wheel")
	),
	createRoute(
		"kingscup", 
		"Kingscup/index.vue", 
		() => import("@/pages/Kingscup/index.vue"),
		() => import("remote_app/kingscup")
	),
	createRoute(
		"ever", 
		"Ever/index.vue", 
		() => import("@/pages/Ever/index.vue"),
		() => import("remote_app/ever")
	)
];

function getRoutes(mode='direct') {
	if ( mode=='direct' ) 
		return routes.map(r => ({
			path: r.path,
			component: r.component,
		}));
	if ( mode=='remote' )
		return routes.map(r => ({
			path: r.path,
			component: r.remoteComponent
		}));
	if ( mode=='config' ) {
		const routeMap = {};
		routes.forEach(r => {
			routeMap['./'+r.name] = r.remotePath;
		})
		return routeMap;
	}
}

export default getRoutes;